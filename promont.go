package main

import (
 "database/sql"
 _ "github.com/go-sql-driver/mysql"
 "fmt"
 "github.com/tkanos/gonfig"
 "os"
 "bufio"
 "crypto/md5"
 "io"
 "github.com/go-redis/redis"
)

type Configuration struct {
  User string
  Host string
  Pass string
  Db string
}


func redisClient() (*redis.Client) {
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", 
		DB:       0,  
	})

	//pong, err := client.Ping().Result()
	//fmt.Println(pong, err)
	return client
	// Output: PONG <nil>
}


func setRedisHash(client *redis.Client, hash string) {
    fmt.Println(hash)

    err := client.Set("hash", hash, 0).Err()
	if err != nil {
		panic(err)
	}
  
	
}

func getRedisHash(client *redis.Client) (string){
  val, err := client.Get("hash").Result()
	if err != nil {
		setRedisHash(client, "")

	}
  return val
}


// go get -u github.com/go-sql-driver/mysql

func hash(emails []string) (hash string){
  h := md5.New()

  for _,email := range emails {
   io.WriteString(h, email)
  }


  return fmt.Sprintf("%x", h.Sum(nil))
}

func writeToFile(emails []string){
    f, err := os.Create("/etc/rspamd/promont.lst")
    if err != nil {
        fmt.Println(err)
        return
    }

    // Create a new writer.
    w := bufio.NewWriter(f)

    for _,element := range emails {
	w.WriteString(element+"\n")
	fmt.Println(element)
     }
    w.Flush()
    f.Close()
}

func main() {

    conf := Configuration{}
    err := gonfig.GetConf("/etc/promont.json", &conf)

    db, err := sql.Open("mysql", conf.User+":"+conf.Pass+"@tcp("+conf.Host+":3306)/"+conf.Db)
  

    

     if err != nil {
		panic(err.Error())  
	}


     err = db.Ping()	

//     rows, err := db.Query("SELECT count(*) as cnt FROM emails")
//     var cnt int
//     rows.Next()
//     err = rows.Scan(&cnt)
//     fmt.Println(cnt)


     var emails []string

     rows2, err := db.Query("SELECT * FROM emails")
     for rows2.Next() {
         var id int
         var email string
         err = rows2.Scan(&id, &email)
         emails = append(emails,email)
     }
	defer db.Close()

    client := redisClient()      
    newHash := hash(emails)
    oldHash := getRedisHash(client)


    if newHash != oldHash {
     setRedisHash(client, newHash)
     fmt.Println(oldHash)
      writeToFile(emails)	
    }
      


}


